use crate::{
    error::Result,
    game_state::menu_fields::MenuFields,
    game_state::GameState,
    ports::{
        controls::{Controls, Input},
        visuals::Visuals,
    },
};

pub fn menu_state<C: Controls, V: Visuals>(
    controls: &C,
    visuals: &V,
    menu_fields: MenuFields,
) -> Result<GameState> {
    menu_fields.display(visuals)?;
    if let Some(stroke) = controls.wait_for_input()? {
        let game_state = match stroke {
            Input::Quit => GameState::Quit,
            Input::StartStop => activate_menu_item(menu_fields),
            other => control_cursor(other, menu_fields),
        };
        Ok(game_state)
    } else {
        Ok(GameState::Menu(menu_fields))
    }
}

fn activate_menu_item(menu_fields: MenuFields) -> GameState {
    if let Some(item) = menu_fields
        .menu_field
        .grid
        .get(&menu_fields.cursor_location)
    {
        item.change_state()
    } else {
        GameState::Menu(menu_fields)
    }
}

fn control_cursor(input: Input, mut menu_fields: MenuFields) -> GameState {
    menu_fields.cursor_location = match input {
        Input::CursorUp => menu_fields.cursor_location.up(),
        Input::CursorDown => menu_fields.cursor_location.down(),
        Input::CursorLeft => menu_fields.cursor_location.left(),
        Input::CursorRight => menu_fields.cursor_location.right(),
        _ => menu_fields.cursor_location,
    };
    GameState::Menu(menu_fields)
}
