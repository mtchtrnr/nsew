use crate::{
    error::Result,
    field::{Coord, Field},
    game_state::{GameState, LevelFields},
    item::{cursor::Cursor, menu_item::MenuItem, text::Text, Item},
    ports::visuals::Visuals,
};

#[derive(Clone)]
pub struct MenuFields {
    menu_text: Field<Text>,
    pub menu_field: Field<MenuItem>,
    pub cursor_location: Coord,
    cursor: Cursor,
}

impl MenuFields {
    pub fn new() -> Self {
        let mut menu_text = Field::new();
        menu_text
            .grid
            .insert(Coord::new(-4, -2), "Pick a level to play:".into());
        let menu_field = levels_to_menu(&vec!["demo1", "demo2", "demo3"]);

        MenuFields {
            menu_text,
            menu_field,
            cursor_location: Coord::new(0, 0),
            cursor: Cursor::new(" ".to_string()),
        }
    }

    pub fn display<T: Visuals>(&self, visuals: &T) -> Result<()> {
        visuals.clear()?;
        visuals.display_field(&self.menu_field)?;
        visuals.display_item(&self.cursor, &self.cursor_location)?;
        visuals.display_field(&self.menu_text)?;
        visuals.present()?;
        Ok(())
    }
}

fn levels_to_menu(paths: &Vec<&str>) -> Field<MenuItem> {
    let mut menu_field = Field::new();
    let mut count = 0;
    for p in paths.iter() {
        let x = 1 + (count / 5) * 3;
        let y = 1 + (count % 5) * 2;
        count += 1;
        let menu_item = level_menu_item(&format!("{}", count), p);
        menu_field.grid.insert(Coord::new(x, y), menu_item);
    }
    menu_field
}

fn level_menu_item(symbol: &str, path: &str) -> MenuItem {
    let level_fields = LevelFields::from_file(path).unwrap();
    let state_change = GameState::Control(level_fields);
    MenuItem::with_change_state(symbol.to_string(), state_change)
}
