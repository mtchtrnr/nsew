use crate::{
    error::Result,
    field::Coord,
    game_state::level_fields::LevelFields,
    game_state::GameState,
    item::{text::Text, Item},
    ports::{
        controls::{Controls, Input},
        visuals::Visuals,
    },
};

pub fn completed_state<C: Controls, V: Visuals>(
    controls: &C,
    visuals: &V,
    level_fields: LevelFields,
) -> Result<GameState> {
    level_fields.display(visuals)?;
    visuals.display_item(&Text::new("Success!".to_string()), &Coord::new(-2, -2))?;
    visuals.present()?;
    if let Some(stroke) = controls.wait_for_input()? {
        let game_state = match stroke {
            Input::Quit => GameState::BuildMenu,
            _ => GameState::Complete(level_fields),
        };
        Ok(game_state)
    } else {
        Ok(GameState::Complete(level_fields))
    }
}
