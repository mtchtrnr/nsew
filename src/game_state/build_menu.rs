use crate::{error::Result, game_state::menu_fields::MenuFields, game_state::GameState};

pub fn build_menu_state() -> Result<GameState> {
    let menu_field = MenuFields::new();
    Ok(GameState::Menu(menu_field))
}
