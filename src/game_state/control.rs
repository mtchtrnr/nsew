use crate::{
    error::Result,
    game_state::{level_fields::LevelFields, GameState},
    instructions::Instructions,
    item::{
        input_instructions::{InputInstruction, InputInstructionType},
        Item,
    },
    ports::{
        controls::{Controls, Input},
        visuals::Visuals,
    },
};

pub fn control_state<C: Controls, V: Visuals>(
    controls: &C,
    visuals: &V,
    level_fields: LevelFields,
) -> Result<GameState> {
    level_fields.display(visuals)?;
    if let Some(stroke) = controls.wait_for_input()? {
        let game_state = match stroke {
            Input::Quit => GameState::BuildMenu,
            Input::StartStop => GameState::Play(level_fields),
            other => control_cursor(other, level_fields),
        };
        Ok(game_state)
    } else {
        // TODO: Do we need this? Maybe `wait_for_input` doesn't need to be an `Option`
        Ok(GameState::Control(level_fields))
    }
}

fn control_cursor(input: Input, mut level_fields: LevelFields) -> GameState {
    let mut instructions = Instructions::new();
    match input {
        Input::CursorUp => instructions.cursor.up = true,
        Input::CursorDown => instructions.cursor.down = true,
        Input::CursorLeft => instructions.cursor.left = true,
        Input::CursorRight => instructions.cursor.right = true,
        Input::AddNorth => input_instruction("n", &mut level_fields),
        Input::AddSouth => input_instruction("s", &mut level_fields),
        Input::AddEast => input_instruction("e", &mut level_fields),
        Input::AddWest => input_instruction("w", &mut level_fields),
        Input::Remove => remove_instruction(&mut level_fields),
        _ => {}
    };
    level_fields.cursor_field.update(&instructions);
    GameState::Control(level_fields)
}

fn input_instruction(symbol: &str, level_fields: &mut LevelFields) {
    for (cursor_loc, _) in level_fields.cursor_field.grid.iter() {
        if let Some(item) = level_fields.input_instruction_field.grid.get(cursor_loc) {
            match item.get_type() {
                InputInstructionType::Subject => {}
                _ => {
                    level_fields
                        .input_instruction_field
                        .grid
                        .insert(*cursor_loc, InputInstruction::new(symbol.to_string()));
                }
            }
        }
    }
}

fn remove_instruction(level_fields: &mut LevelFields) {
    for (cursor_loc, _) in level_fields.cursor_field.grid.iter() {
        if let Some(item) = level_fields.input_instruction_field.grid.get(cursor_loc) {
            match item.get_type() {
                InputInstructionType::Subject => {}
                _ => {
                    level_fields
                        .input_instruction_field
                        .grid
                        .insert(*cursor_loc, InputInstruction::empty());
                }
            }
        }
    }
}
