use crate::{
    error::Result,
    field::{Coord, Field},
    game_state::{level_fields::LevelFields, GameState},
    instructions::{grid_to_kite_instructions, Instructions},
    item::{goal::Goal, kite::Kite, Item},
    ports::{
        controls::{Controls, Input},
        visuals::Visuals,
    },
};
use std::time::Duration;

pub fn play_state<C: Controls, V: Visuals>(
    controls: &C,
    visuals: &V,
    mut level_fields: LevelFields,
) -> Result<GameState> {
    level_fields.display(visuals)?;
    let mut instructions = Instructions::new();
    instructions.kite = grid_to_kite_instructions(&level_fields.input_instruction_field.grid);
    level_fields.kite_field.update(&instructions);
    if let Some(stroke) = controls.wait_for_input_with_timeout(wait_duration())? {
        let game_state = match stroke {
            Input::StartStop | Input::Quit => {
                level_fields.refresh_kites()?;
                GameState::Control(level_fields)
            }
            _ => GameState::Play(level_fields), // Will prematurely advance frame
        };
        Ok(game_state)
    } else if level_completed(&level_fields) {
        Ok(GameState::Complete(level_fields))
    } else {
        Ok(GameState::Play(level_fields))
    }
}

fn level_completed(level_fields: &LevelFields) -> bool {
    level_fields
        .goal_field
        .grid
        .iter()
        .all(|(coord, goal)| goal_is_met(coord, goal, &level_fields.kite_field))
}

fn goal_is_met(coord: &Coord, goal: &Goal, kites: &Field<Kite>) -> bool {
    if let Some(kite) = kites.grid.get(coord) {
        kite.get_symbol() == goal.get_symbol()
    } else {
        false
    }
}

// TODO: Parameterize
const REFRESH_RATE_PER_SECOND: f32 = 2.;

fn wait_duration() -> Duration {
    let secs = 1.0 / REFRESH_RATE_PER_SECOND;
    let millis = (secs * 1000.) as u64;
    Duration::from_millis(millis)
}
