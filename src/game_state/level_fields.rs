use crate::{
    error::Result,
    field::{Coord, Field, Grid},
    item::{cursor::Cursor, goal::Goal, input_instructions::InputInstruction, kite::Kite, Item},
    level_builder::field_from_file,
    ports::visuals::Visuals,
};
use std::collections::HashMap;

#[derive(Clone)]
pub struct LevelFields {
    path: String,
    pub goal_field: Field<Goal>,
    pub kite_field: Field<Kite>,
    pub input_instruction_field: Field<InputInstruction>,
    pub cursor_field: Field<Cursor>,
}

impl LevelFields {
    pub fn from_file(path: &str) -> Result<Self> {
        let level_fields = LevelFields {
            path: path.to_string(),
            goal_field: new_goal_field(path)?,
            kite_field: new_kite_field(path)?,
            input_instruction_field: new_input_instruction_field(path)?,
            cursor_field: new_cursor_field()?,
        };
        Ok(level_fields)
    }

    pub fn refresh_kites(&mut self) -> Result<()> {
        self.kite_field = new_kite_field(&self.path)?;
        Ok(())
    }

    pub fn display<T: Visuals>(&self, visuals: &T) -> Result<()> {
        visuals.clear()?;
        visuals.display_field(&self.goal_field)?;
        visuals.display_field(&self.kite_field)?;
        visuals.display_field(&self.input_instruction_field)?;
        visuals.display_field(&self.cursor_field)?;
        visuals.present()?;
        Ok(())
    }
}

// Holdover helper functions

fn new_cursor_field() -> Result<Field<Cursor>> {
    let cursor = Cursor::new("6".to_string());
    let mut grid = Grid::new();
    grid.insert(Coord::new(0, 0), cursor);
    Ok(Field {
        grid,
        already: HashMap::new(),
    })
}

fn level_dir(level: &str, file: &str) -> String {
    format!("levels/{}/{}", level, file)
}

fn new_kite_field(path: &str) -> Result<Field<Kite>> {
    field_from_file(&level_dir(path, "kites"))
}

fn new_goal_field(path: &str) -> Result<Field<Goal>> {
    field_from_file(&level_dir(path, "goal"))
}

fn new_input_instruction_field(path: &str) -> Result<Field<InputInstruction>> {
    field_from_file(&level_dir(path, "instructions"))
}
