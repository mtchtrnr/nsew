use crate::{
    error::Result,
    game_state::{
        build_menu::build_menu_state, completed::completed_state, control::control_state,
        menu::menu_state, play::play_state, GameState,
    },
    ports::{controls::Controls, visuals::Visuals},
};

pub struct NSEW<V: Visuals, C: Controls> {
    visuals: V,
    controls: C,
}

impl<D: Visuals, I: Controls> NSEW<D, I> {
    pub fn new(display: D, input: I) -> Self {
        NSEW {
            visuals: display,
            controls: input,
        }
    }

    // TODO: Consider having visuals and controls tracked with game state rather than having `run`
    //  be a method of NSEW
    pub fn run(&self) -> Result<()> {
        let mut game_state = GameState::BuildMenu;
        loop {
            game_state = match game_state {
                GameState::BuildMenu => build_menu_state()?,
                GameState::Menu(menu_field) => {
                    menu_state(&self.controls, &self.visuals, menu_field)?
                }
                GameState::Control(level_fields) => {
                    control_state(&self.controls, &self.visuals, level_fields)?
                }
                GameState::Play(level_fields) => {
                    play_state(&self.controls, &self.visuals, level_fields)?
                }
                GameState::Complete(level_fields) => {
                    completed_state(&self.controls, &self.visuals, level_fields)?
                }
                GameState::Quit => break,
            }
        }
        Ok(())
    }
}
