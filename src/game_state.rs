pub mod build_menu;
pub mod completed;
pub mod control;
pub mod menu;
pub mod play;

pub mod level_fields;
pub mod menu_fields;

use crate::game_state::{level_fields::LevelFields, menu_fields::MenuFields};

#[derive(Clone)]
pub enum GameState {
    BuildMenu,
    Menu(MenuFields),
    Control(LevelFields),
    Play(LevelFields),
    Complete(LevelFields),
    Quit,
}
