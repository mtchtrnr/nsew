pub mod cursor;
pub mod goal;
pub mod input_instructions;
pub mod kite;
pub mod menu_item;
pub mod text;

use crate::{
    field::{Coord, Grid},
    instructions::Instructions,
    ports::visuals::Color,
};

// TODO: Would the move_* methods be better off static? Only Kite is using `self`, and just to get symbol
// TODO: We might just want to get rid of the move methods, cause they are only used by a couple items, might
//  be bad abstraction... but I still kinda like the idea of everything being move-able.
pub trait Item: Sized + Clone {
    fn new(symbol: String) -> Self;
    fn get_color(&self) -> Color;
    fn get_background_color(&self) -> Color {
        Color::Default
    }
    fn get_symbol(&self) -> &str;
    fn move_north(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord>;
    fn move_south(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord>;
    fn move_east(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord>;
    fn move_west(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord>;
}
