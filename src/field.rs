use crate::instructions::Instructions;
use crate::item::Item;
use std::collections::HashMap;

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Coord {
    pub x: i16,
    pub y: i16,
}

impl Coord {
    pub fn new(x: i16, y: i16) -> Self {
        Coord { x, y }
    }
    pub fn up(self) -> Self {
        Coord::new(self.x, self.y - 1)
    }
    pub fn down(self) -> Self {
        Coord::new(self.x, self.y + 1)
    }
    pub fn right(self) -> Self {
        Coord::new(self.x + 1, self.y)
    }
    pub fn left(self) -> Self {
        Coord::new(self.x - 1, self.y)
    }
}

pub type Grid<T> = HashMap<Coord, T>;

pub type Movements = Vec<(Coord, Coord)>;

#[derive(Debug, Clone)]
pub struct Field<T: Item> {
    pub grid: Grid<T>,
    /// Track what has already moved
    pub already: HashMap<Coord, bool>,
}

impl<T: Item> Field<T> {
    pub fn new() -> Self {
        Self {
            grid: Grid::<T>::new(),
            already: HashMap::new(),
        }
    }
}

impl<T: Item> Field<T> {
    fn north(&self, instructions: &Instructions) -> Movements {
        let mut movements = Movements::new();
        for (&start, item) in self.grid.iter() {
            if let Some(finish) = item.move_north(start, instructions, &self.grid) {
                movements.push((start, finish));
            }
        }
        movements
    }
    fn south(&self, instructions: &Instructions) -> Movements {
        let mut movements = Movements::new();
        for (&start, item) in self.grid.iter() {
            if let Some(finish) = item.move_south(start, instructions, &self.grid) {
                movements.push((start, finish));
            }
        }
        movements
    }
    fn east(&self, instructions: &Instructions) -> Movements {
        let mut movements = Movements::new();
        for (&start, item) in self.grid.iter() {
            if let Some(finish) = item.move_east(start, instructions, &self.grid) {
                movements.push((start, finish));
            }
        }
        movements
    }
    fn west(&self, instructions: &Instructions) -> Movements {
        let mut movements = Movements::new();
        for (&start, item) in self.grid.iter() {
            if let Some(finish) = item.move_west(start, instructions, &self.grid) {
                movements.push((start, finish));
            }
        }
        movements
    }
    fn execute_movements(&mut self, movements: Movements) {
        for (start, finish) in movements {
            // Only move items that haven't moved already
            if !self.already.contains_key(&start) {
                if let Some(item) = self.grid.remove(&start) {
                    self.grid.insert(finish, item);
                    self.already.insert(finish, true);
                }
            }
        }
    }

    fn renew(&mut self) {
        self.already = HashMap::new()
    }

    pub fn update(&mut self, instrctions: &Instructions) {
        self.execute_movements(self.north(instrctions));
        self.execute_movements(self.south(instrctions));
        self.execute_movements(self.east(instrctions));
        self.execute_movements(self.west(instrctions));
        self.renew();
    }
}
