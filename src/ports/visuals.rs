use crate::{
    error::Result,
    field::{Coord, Field},
    item::Item,
};

#[derive(Copy, Clone, Debug)]
pub enum Color {
    Red,
    Green,
    Blue,
    Cyan,
    Magenta,
    Yellow,
    White,
    Black,
    Default,
}

pub trait Visuals {
    fn clear(&self) -> Result<()>;
    fn display_field(&self, field: &Field<impl Item>) -> Result<()> {
        for (coord, item) in field.grid.iter() {
            self.display_item(item, coord)?;
        }
        Ok(())
    }
    fn display_item<T: Item>(&self, item: &T, coord: &Coord) -> Result<()>;
    fn present(&self) -> Result<()>;
}

// TODO: Make into a mock
pub struct DummyVisuals;

impl Visuals for DummyVisuals {
    fn clear(&self) -> Result<()> {
        Ok(())
    }

    fn display_item<T: Item>(&self, _item: &T, _coord: &Coord) -> Result<()> {
        Ok(())
    }

    fn present(&self) -> Result<()> {
        Ok(())
    }
}
