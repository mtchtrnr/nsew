use crate::error::Result;
use std::time::Duration;

pub enum Input {
    StartStop,
    CursorUp,
    CursorDown,
    CursorLeft,
    CursorRight,
    AddNorth,
    AddSouth,
    AddEast,
    AddWest,
    Remove,
    Quit,
}

pub trait Controls {
    fn wait_for_input(&self) -> Result<Option<Input>>;
    fn wait_for_input_with_timeout(&self, duration: Duration) -> Result<Option<Input>>;
}

// TODO: Make into a mock
pub struct DummyControls;

impl Controls for DummyControls {
    fn wait_for_input(&self) -> Result<Option<Input>> {
        Ok(Some(Input::StartStop))
    }

    fn wait_for_input_with_timeout(&self, _duration: Duration) -> Result<Option<Input>> {
        Ok(None)
    }
}
