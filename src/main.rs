mod adapters;
mod error;
mod field;
mod game_state;
mod instructions;
mod item;
mod level_builder;
mod nsew;
mod ports;

use crate::{
    adapters::{rb_controls::RBInput, rb_visuals::RBDisplay},
    error::Result,
    nsew::NSEW,
    // ports::{controls::DummyControls, visuals::DummyVisuals},
};

use rustbox::RustBox;
use std::{default::Default, sync::Arc};

fn main() -> Result<()> {
    let rustbox = Arc::new(RustBox::init(Default::default())?);
    let display = RBDisplay::new(rustbox.clone(), 5, 2);
    let input = RBInput::new(rustbox);

    // let display = DummyVisuals;
    // let input = DummyControls;
    let nsew = NSEW::new(display, input);
    nsew.run()
}
