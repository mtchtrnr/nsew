use crate::{
    error::{Error, Result},
    field::{Coord, Field, Grid},
    item::Item,
};
use std::{collections::HashMap, fs::File, io::Read};

pub fn field_from_file<T: Item>(path: &str) -> Result<Field<T>> {
    let mut file = File::open(path)?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;
    let field = field_from_string(&buffer)?;
    Ok(field)
}

fn stencil_is_valid_field(stencil: &str) -> bool {
    let lines: Vec<&str> = stencil.split('\n').collect();
    let width = lines[0].len();
    lines.iter().all(|a| a.len() == width)
}

fn field_from_string<T: Item>(value: &str) -> Result<Field<T>> {
    if stencil_is_valid_field(value) {
        let lines: Vec<Vec<&str>> = value
            .split('\n')
            .map(|line| {
                line.split_terminator("")
                    .skip(1)
                    .filter(|&l| l != "")
                    .collect::<Vec<&str>>()
            })
            .collect();
        let width = lines[0].len();
        let height = lines.len();
        let mut grid = Grid::new();
        for (y, line) in lines.iter().enumerate().take(height) {
            for (x, &letter) in line.iter().enumerate().take(width) {
                if letter != "." && letter != " " {
                    let item = T::new(letter.to_string());
                    grid.insert(Coord::new(x as i16, y as i16), item);
                }
            }
        }
        Ok(Field {
            grid,
            already: HashMap::new(),
        })
    } else {
        Err(Error::LevelBuilder(format!(
            "Cannot make field from string: {}",
            value
        )))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::item::kite::Kite;

    #[test]
    fn rectangle_string_is_valid_field() {
        let stencil = "....\n....\n..K.\n....\n....";
        assert!(field_from_string::<Kite>(stencil).is_ok())
    }

    #[test]
    fn non_rectangle_string_is_not_valid_field() {
        let stencil = "....\n....\nK\n....\n....";
        assert!(field_from_string::<Kite>(stencil).is_err())
    }

    #[test]
    fn can_make_level_from_file() {
        let path = "levels/test1/kites";
        let level = field_from_file::<Kite>(path);
        assert!(level.is_ok())
    }
}
