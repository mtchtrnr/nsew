use crate::{
    error::Result,
    ports::controls::{Controls, Input},
};
use rustbox::{Key, RustBox};
use std::{sync::Arc, time::Duration};

pub struct RBInput {
    rustbox: Arc<RustBox>,
}

impl RBInput {
    pub fn new(rustbox: Arc<RustBox>) -> RBInput {
        RBInput { rustbox }
    }

    fn match_event(event: rustbox::Event) -> Option<Input> {
        match event {
            rustbox::Event::KeyEvent(key) => match key {
                Key::Char(' ') | Key::Enter => Some(Input::StartStop),
                Key::Char('k') | Key::Up => Some(Input::CursorUp),
                Key::Char('j') | Key::Down => Some(Input::CursorDown),
                Key::Char('h') | Key::Left => Some(Input::CursorLeft),
                Key::Char('l') | Key::Right => Some(Input::CursorRight),
                Key::Char('n') => Some(Input::AddNorth),
                Key::Char('s') => Some(Input::AddSouth),
                Key::Char('e') => Some(Input::AddEast),
                Key::Char('w') => Some(Input::AddWest),
                Key::Char('x') => Some(Input::Remove),
                Key::Char('q') | Key::Esc => Some(Input::Quit),
                _ => None,
            },
            _ => None,
        }
    }
}
impl Controls for RBInput {
    fn wait_for_input(&self) -> Result<Option<Input>> {
        let event = self.rustbox.poll_event(false)?;
        Ok(Self::match_event(event))
    }

    fn wait_for_input_with_timeout(&self, duration: Duration) -> Result<Option<Input>> {
        let event = self.rustbox.peek_event(duration, false)?;
        let maybe_event = Self::match_event(event);

        Ok(maybe_event)
    }
}
