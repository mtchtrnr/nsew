use crate::{
    error::Result,
    field::Coord,
    item::Item,
    ports::visuals::{Color, Visuals},
};
use rustbox::{Color as RBColor, RustBox};
use std::sync::Arc;

impl From<Color> for RBColor {
    fn from(c: Color) -> RBColor {
        match c {
            Color::Red => RBColor::Red,
            Color::Green => RBColor::Green,
            Color::Blue => RBColor::Blue,
            Color::Magenta => RBColor::Magenta,
            Color::Cyan => RBColor::Cyan,
            Color::Yellow => RBColor::Yellow,
            Color::White => RBColor::White,
            Color::Black => RBColor::Black,
            Color::Default => RBColor::Default,
        }
    }
}

pub struct RBDisplay {
    rustbox: Arc<RustBox>,
    side_border: i16,
    top_border: i16,
}

impl RBDisplay {
    pub fn new(rustbox: Arc<RustBox>, side_border: i16, top_border: i16) -> Self {
        RBDisplay {
            rustbox,
            side_border,
            top_border,
        }
    }
}

impl Visuals for RBDisplay {
    fn clear(&self) -> Result<()> {
        self.rustbox.clear();
        Ok(())
    }

    fn display_item<T: Item>(&self, item: &T, coord: &Coord) -> Result<()> {
        self.rustbox.print(
            (coord.x + self.side_border) as usize,
            (coord.y + self.top_border) as usize,
            rustbox::RB_BOLD,
            item.get_color().into(),
            item.get_background_color().into(),
            item.get_symbol(),
        );
        Ok(())
    }

    fn present(&self) -> Result<()> {
        self.rustbox.present();
        Ok(())
    }
}
