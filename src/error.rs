use derive_more::Display;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Error, Debug, Display)]
pub enum Error {
    LevelBuilder(String),
    IO(#[from] std::io::Error),
    RustBox(#[from] rustbox::InitError),
    Controls(#[from] rustbox::EventError),
    _NotReachable,
}
