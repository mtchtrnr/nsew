use crate::{
    field::{Coord, Grid},
    instructions::Instructions,
    item::Item,
    ports::visuals::Color,
};

#[derive(Clone)]
pub struct Text {
    symbol: String,
}

impl Item for Text {
    fn new(symbol: String) -> Self {
        symbol.into()
    }

    fn get_color(&self) -> Color {
        Color::White
    }

    fn get_symbol(&self) -> &str {
        &self.symbol
    }

    fn move_north(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_south(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_east(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_west(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }
}

impl<T: ToString> From<T> for Text {
    fn from(text: T) -> Self {
        Text {
            symbol: text.to_string(),
        }
    }
}
