use crate::{
    field::{Coord, Grid},
    instructions::Instructions,
    item::Item,
    ports::visuals::Color,
};

#[derive(Copy, Clone)]
pub enum InputInstructionType {
    Subject,
    Relation,
    Empty,
}

#[derive(Clone)]
pub struct InputInstruction {
    // TODO: This is confusing for the Empty case especially.
    //  Prolly move it to enum fields and maybe just make the struct into the enum?
    symbol: String,
    type_: InputInstructionType,
}

impl InputInstruction {
    pub fn empty() -> Self {
        InputInstruction {
            symbol: "_".to_string(),
            type_: InputInstructionType::Empty,
        }
    }

    pub fn get_type(&self) -> InputInstructionType {
        self.type_
    }
}

impl Item for InputInstruction {
    fn new(symbol: String) -> Self {
        let type_ = match symbol.as_ref() {
            "_" => InputInstructionType::Empty,
            _ => {
                if symbol.chars().next().unwrap().is_lowercase() {
                    InputInstructionType::Relation
                } else {
                    InputInstructionType::Subject
                }
            }
        };
        InputInstruction { symbol, type_ }
    }

    fn get_color(&self) -> Color {
        match self.type_ {
            InputInstructionType::Subject => Color::Cyan,
            InputInstructionType::Relation => Color::Magenta,
            InputInstructionType::Empty => Color::White,
        }
    }

    fn get_symbol(&self) -> &str {
        match &self.type_ {
            InputInstructionType::Empty => "_",
            _ => &self.symbol,
        }
    }

    fn move_north(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_south(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_east(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_west(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }
}
