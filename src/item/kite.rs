use crate::{
    field::{Coord, Grid},
    instructions::{Direction, Instructions, SymbolInstructions},
    item::Item,
    ports::visuals::Color,
};

#[derive(Clone, Debug)]
pub struct Kite {
    symbol: String,
}

impl Kite {
    fn try_move(grid: &Grid<Self>, to: Coord) -> Option<Coord> {
        if !grid.contains_key(&to) {
            Some(to)
        } else {
            None
        }
    }

    fn move_dir(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
        dir: Direction,
        to: Coord,
    ) -> Option<Coord> {
        let any_loc_is_occupied = |i: &SymbolInstructions| {
            i.dir_if(dir)
                .iter()
                .any(|rp| grid.contains_key(&rp.from_coord(location)))
        };
        if instructions
            .kite
            .for_symbol(&self.symbol)
            .map(any_loc_is_occupied)
            .unwrap_or(false)
        {
            Self::try_move(grid, to)
        } else {
            None
        }
    }
}

impl Item for Kite {
    fn new(symbol: String) -> Self {
        Kite { symbol }
    }

    fn get_color(&self) -> Color {
        Color::Green
    }

    fn get_symbol(&self) -> &str {
        &self.symbol
    }

    fn move_north(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord> {
        self.move_dir(
            location,
            instructions,
            grid,
            Direction::North,
            location.up(),
        )
    }

    fn move_south(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord> {
        self.move_dir(
            location,
            instructions,
            grid,
            Direction::South,
            location.down(),
        )
    }

    fn move_east(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord> {
        self.move_dir(
            location,
            instructions,
            grid,
            Direction::East,
            location.right(),
        )
    }

    fn move_west(
        &self,
        location: Coord,
        instructions: &Instructions,
        grid: &Grid<Self>,
    ) -> Option<Coord> {
        self.move_dir(
            location,
            instructions,
            grid,
            Direction::West,
            location.left(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::instructions::RelativePosition;

    #[test]
    fn do_not_move_when_alone() {
        let kite = Kite::new("lol".to_string());
        let kite_loc = Coord::new(0, 0);
        let mut instructions = Instructions::new();
        let mut a = SymbolInstructions::new();
        a.add_instruction(Direction::West, RelativePosition::new(-1, 1));
        instructions.kite.for_symbols.insert("lol".to_string(), a);
        let mut grid = Grid::new();
        grid.insert(kite_loc.clone(), kite.clone());
        let maybe_movement = kite.move_west(kite_loc, &instructions, &grid);
        assert!(maybe_movement.is_none());
    }

    #[test]
    fn move_west_when_rel_pos_is_filled() {
        let kite = Kite::new("lol".to_string());
        let kite_loc = Coord::new(0, 0);
        let other = Kite::new("moo".to_string());
        let other_loc = Coord::new(1, -1);
        let mut instructions = Instructions::new();
        let mut a = SymbolInstructions::new();
        a.add_instruction(Direction::North, RelativePosition::new(1, 1));
        instructions.kite.for_symbols.insert("lol".to_string(), a);
        let mut grid = Grid::new();
        grid.insert(kite_loc.clone(), kite.clone());
        grid.insert(other_loc.clone(), other.clone());
        // let expected = Coord::new(0, 1);
        let expected = kite_loc.up();
        let actual = kite.move_north(kite_loc, &instructions, &grid).unwrap();
        assert_eq!(expected, actual);
    }
}
