use crate::game_state::GameState;
use crate::instructions::Instructions;
use crate::{
    field::{Coord, Grid},
    item::Item,
    ports::visuals::Color,
};

#[derive(Clone)]
pub struct MenuItem {
    text: String,
    // TODO: We can generate this dynamically, but this works for now
    change_state: GameState,
}

impl MenuItem {
    pub fn with_change_state(text: String, change_state: GameState) -> Self {
        Self { text, change_state }
    }

    pub fn change_state(&self) -> GameState {
        self.change_state.clone()
    }
}

impl Item for MenuItem {
    fn new(_symbol: String) -> Self {
        unimplemented!()
    }

    fn get_color(&self) -> Color {
        Color::White
    }

    fn get_symbol(&self) -> &str {
        &self.text
    }

    fn move_north(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_south(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_east(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }

    fn move_west(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        unimplemented!()
    }
}
