use crate::{
    field::{Coord, Grid},
    instructions::Instructions,
    item::Item,
    ports::visuals::Color,
};

#[derive(Clone)]
pub struct Goal {
    symbol: String,
}

impl Item for Goal {
    fn new(symbol: String) -> Self {
        Goal { symbol }
    }

    fn get_color(&self) -> Color {
        Color::Red
    }

    fn get_symbol(&self) -> &str {
        &self.symbol
    }

    fn move_north(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        None
    }

    fn move_south(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        None
    }

    fn move_east(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        None
    }

    fn move_west(
        &self,
        _location: Coord,
        _instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        None
    }
}
