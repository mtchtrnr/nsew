use crate::{
    field::{Coord, Grid},
    instructions::Instructions,
    item::Item,
    ports::visuals::Color,
};

#[derive(Clone)]
pub struct Cursor {
    // TODO: Prolly don't need this
    symbol: String,
}

impl Item for Cursor {
    fn new(symbol: String) -> Self {
        Cursor { symbol }
    }

    fn get_color(&self) -> Color {
        Color::White
    }
    fn get_background_color(&self) -> Color {
        Color::White
    }
    fn get_symbol(&self) -> &str {
        &self.symbol
    }

    fn move_north(
        &self,
        location: Coord,
        instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        if instructions.cursor.up {
            Some(location.up())
        } else {
            None
        }
    }

    fn move_south(
        &self,
        location: Coord,
        instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        if instructions.cursor.down {
            Some(location.down())
        } else {
            None
        }
    }

    fn move_east(
        &self,
        location: Coord,
        instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        if instructions.cursor.right {
            Some(location.right())
        } else {
            None
        }
    }

    fn move_west(
        &self,
        location: Coord,
        instructions: &Instructions,
        _grid: &Grid<Self>,
    ) -> Option<Coord> {
        if instructions.cursor.left {
            Some(location.left())
        } else {
            None
        }
    }
}
