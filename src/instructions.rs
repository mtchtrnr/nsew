use crate::field::{Coord, Grid};
use crate::item::input_instructions::{InputInstruction, InputInstructionType};
use crate::item::Item;
use std::collections::HashMap;

pub enum Direction {
    North,
    South,
    East,
    West,
}

// TODO: Do these need to be in a shared object anymore? It looks like they are used separately.
#[derive(Clone)]
pub struct Instructions {
    pub cursor: CursorInstructions,
    pub kite: KiteInstructions,
}

impl Instructions {
    pub fn new() -> Instructions {
        Instructions {
            cursor: CursorInstructions::new(),
            kite: KiteInstructions::new(),
        }
    }
}

#[derive(Clone)]
pub struct CursorInstructions {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
}

impl CursorInstructions {
    pub fn new() -> Self {
        CursorInstructions {
            up: false,
            down: false,
            left: false,
            right: false,
        }
    }
}

#[derive(Clone, Debug)]
pub struct KiteInstructions {
    pub for_symbols: HashMap<String, SymbolInstructions>,
}

impl KiteInstructions {
    pub fn new() -> Self {
        KiteInstructions {
            for_symbols: HashMap::new(),
        }
    }
    pub fn for_symbol(&self, symbol: &str) -> Option<&SymbolInstructions> {
        self.for_symbols.get(symbol)
    }
}

/// The position relative to some coordinate e.g. x: 1, y: 1 would be right one and up one
#[derive(Clone, PartialEq, Debug)]
pub struct RelativePosition {
    dx: i16,
    dy: i16,
}

impl RelativePosition {
    pub fn new(dx: i16, dy: i16) -> Self {
        RelativePosition { dx, dy }
    }

    pub fn from_coord(&self, rel_to: Coord) -> Coord {
        Coord::new(rel_to.x + self.dx, rel_to.y - self.dy)
    }
}

/// Instructions to move direction if other kites are in specified relative positions
#[derive(Clone, Debug)]
pub struct SymbolInstructions {
    north_if: Vec<RelativePosition>,
    south_if: Vec<RelativePosition>,
    east_if: Vec<RelativePosition>,
    west_if: Vec<RelativePosition>,
}

impl SymbolInstructions {
    pub fn new() -> Self {
        SymbolInstructions {
            north_if: Vec::new(),
            south_if: Vec::new(),
            east_if: Vec::new(),
            west_if: Vec::new(),
        }
    }

    /// Kite should move direction if the returned relative positions are occupied
    pub fn dir_if(&self, dir: Direction) -> &Vec<RelativePosition> {
        match dir {
            Direction::North => &self.north_if,
            Direction::South => &self.south_if,
            Direction::East => &self.east_if,
            Direction::West => &self.west_if,
        }
    }

    pub fn add_instruction(&mut self, dir: Direction, rel_pos: RelativePosition) {
        let positions_vec = match dir {
            Direction::North => &mut self.north_if,
            Direction::South => &mut self.south_if,
            Direction::East => &mut self.east_if,
            Direction::West => &mut self.west_if,
        };
        positions_vec.push(rel_pos);
    }
}

pub fn grid_to_kite_instructions(grid: &Grid<InputInstruction>) -> KiteInstructions {
    let mut subjects = Vec::new();
    for (loc, item) in grid.iter() {
        match item.get_type() {
            InputInstructionType::Subject => subjects.push((*loc, item.get_symbol())),
            _ => {}
        }
    }
    let mut instructions = KiteInstructions::new();
    for (loc, symbol) in subjects.iter() {
        let symbol_instructions = symbol_instructions_for_subject(*loc, &grid);
        instructions
            .for_symbols
            .insert(symbol.to_string(), symbol_instructions);
    }
    instructions
}

pub fn symbol_instructions_for_subject(
    loc: Coord,
    grid: &Grid<InputInstruction>,
) -> SymbolInstructions {
    let mut instructions = SymbolInstructions::new();
    for x in -2..=2 {
        for y in -2..=2 {
            let rel_pos = RelativePosition::new(x, y);
            let absolute_pos = rel_pos.from_coord(loc);
            if let Some(item) = grid.get(&absolute_pos) {
                match item.get_type() {
                    InputInstructionType::Relation => match item.get_symbol() {
                        "n" => instructions.add_instruction(Direction::North, rel_pos),
                        "s" => instructions.add_instruction(Direction::South, rel_pos),
                        "e" => instructions.add_instruction(Direction::East, rel_pos),
                        "w" => instructions.add_instruction(Direction::West, rel_pos),
                        _ => {}
                    },
                    _ => {}
                }
            }
        }
    }
    instructions
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn relative_position_coord_is_correct() {
        let rel_pos = RelativePosition::new(1, 1);
        let origin = Coord::new(0, 0);
        let expect = Coord::new(1, -1);
        let actual = rel_pos.from_coord(origin);
        assert_eq!(expect, actual);
    }
}
