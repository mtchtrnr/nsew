# NORTH SOUTH EAST WEST

## Install and run

To play you need [Rust](https://www.rust-lang.org/tools/install).

Clone this repo onto your compy.

For now, we're just running it with Cargo. In your console, navigate to the cloned `nsew` directory
and input
```bash
cargo run
```
:)

## Objective

The green letters on the left are kites. You want them to be blown into the red objectives.

The compass roses on the right are where you input instructions for how the wind blows. There should
be a compass for each kite that needs to move. 

Instructions are simple. Move north, move south, move east, or move west. Each `_` around the
compasses is a relative position. Replace the `_` with a letter `n`/`s`/`e`/`w`. If *another* kite
occupies that position relative to the kites that share a symbol the the compass, that represented 
kite will move in the instructed direction!

For example, kite `A`

```
         
   A     
   B     
         

```
with instructions

```
 _____
 _____
 __A__
 __e__
 _____

```
will move `A` East
```
        
    A  
   B    
        

```
Because the instructions say if there is another kite directly
below `A`, it will move `e` (East)


## Controls

### Cursor
`k`/`Up` - up

`j`/`Down` - down

`l`/`Right` - right

`h`/`Left` - left

`n` - Add north instruction

`s` - Add south instruction

`e` - Add east instruction

`w` - Add west instruction

`x` - Remove instruction

`Space`/`Enter` - Select/Play/Stop

`q`/`Esc` - Back/Quit

